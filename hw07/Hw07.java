/*
 * Brian Snyder
 * bts222@lehigh.edu
 * 10/27/18
 * Hw07
 * 
 * This program utilizes various methods to take a user-promted passage, and edit this passage */

import java.util.Scanner;

public class Hw07 {
  
  // method that asks user for a sample text and returns it as a String to the main method
  public static String sampleText() {
    Scanner scrn = new Scanner(System.in);
    System.out.println("Enter a sample text:");
    String text = scrn.nextLine();
    return text;
  }
  
  // method that prints the menu options
  public static void printMenu(String text) {
    Scanner scrn = new Scanner(System.in);
    boolean validInput = false;
    System.out.println("MENU");
    System.out.println("c - Number of non-whitespace characters");
    System.out.println("w - Number of words");
    System.out.println("f - Find text");
    System.out.println("r - Replace all !'s");
    System.out.println("s - Shorten spaces");
    System.out.println("q - Quit");
    System.out.print("Choose an option: ");
    String input = scrn.next();
    // while an invalid option was not entered, the program runs a method corresponding to the letter entered
    while (!validInput) {
      if (input.equals("c") || input.equals("w") || input.equals("f") || input.equals("r") || input.equals("s") || input.equals("q")) {
        System.out.println("Input: " + input);
        // if 'q' is hit, the program exits out while loop
        if (input.equals("q")) {
          validInput = true;
          System.out.println("Quit");
        } else {
          if (input.equals("c")) {
            System.out.println(getNumOfNonWhiteCharacters(text));
          }
          if (input.equals("w")) {
            System.out.println("Number of words: " + getNumOfWords(text));
          }
          if (input.equals("f")) {
            System.out.println("Enter a word or phrase to be found");
            input = scrn.next();
            System.out.println("\"input\"" + " instances: " + findText(text, input));
          }
          if (input.equals("r")) {
            System.out.print("Edited text: " + replaceExclamation(text));
          }
          if (input.equals("s")) {
            System.out.println("Edited text: " + shortenSpaces(text));
          }
          // allows the program to do multiple methods until 'q' is hit to quit
          input = scrn.next();
          continue;
        }
        // if an input was invalid, the user is prompted again
      } else {
        System.out.println("Invalid input, try again");
        System.out.println(input);
        input = scrn.next();
        continue;
      }
    }
  }
  
  // method that returns the number of non-white characters
  public static String getNumOfNonWhiteCharacters(String text) {
    int count = 0;
    // finds # of white spaces
    for (int i = 0; i < text.length(); i++) {
      if (text.substring(i,i+1).equals(" ")) {
        count++;
      }
    }
    // subtracts length of all characters by # of white spaces
    int numOfNonWhiteSpaces = text.length() - count;
    String num = "" + numOfNonWhiteSpaces;
    return num;
  }
  
  // method that returns the # of words in the string
  public static  int getNumOfWords(String text) {
    int wordCount = 0;
    boolean isAWord = false;
    // loops through string and uses a counter and the characters used to determine the # of words
    // not this method counts conjuctions as two words (example We'll will be considered 2 words)
    // this could be changed by seeing if charAt(i) == ' ' instead, but in this case I decided to
    // consider conjuctions multiple words
    for (int i = 0; i < text.length(); i++) {
      // is a valid character is enterd and not at the end, it's considerd a word
      if (Character.isLetter(text.charAt(i)) && i != text.length()-1) {
        isAWord = true;
        // otherwise if an invalid character is entered and it's a word, the word count increases
        // in other words if a word ends, count increases
      } else if (!Character.isLetter(text.charAt(i)) && isAWord) {
        wordCount++;
        isAWord = false;
        // if word ends and at the end, count increases as well
      } else if (!Character.isLetter(text.charAt(i)) && i == text.length()-1) {
        wordCount++;
      }
    }
    return wordCount;
  }
  
  // method that finds a user entered word/phrase inside string and returns # of times it appears
  public static int findText(String text, String word) {
    int count = 0;
    int index = text.indexOf(word);
    // as long as there is an index of where the word is found, increase counter, and make text a substring
    // that cuts this word out, and then check if there's another index of this word/phrase
    while (index != -1) {
      count++;
      text = text.substring(index + word.length());
      index = text.indexOf(word);      
    }
    return count;
  }
  
  // method that replaces all exclamation points in the code with "."
  public static String replaceExclamation(String text) {
    // finds indexes of "!", and makes text a substring where "!" becomes replaced with "."
    int index = text.indexOf("!");
    // same concept that's done above in findText()
    while (index != -1) {
      text = text.substring(0,index) + "." + text.substring(index+1);
      index = text.indexOf("!");
    }
    return text;
  }
  
  // method that removes multiple spaces in the text
  public static String shortenSpaces(String text) {
    int count = 0;
    // checks for spaces, and if a space is found right after, the text is modified by using 
    // substring to remove this extra space
    for (int i = 0; i < text.length(); i++) {
      if (text.substring(i,i+1).equals(" ")) {
        if (text.substring(i+1,i+2).equals(" ")) {
          text = text.substring(0,i+1) + text.substring(i+2);
        }
      }
    }
    return text;
  }
  
  // main method
  public static void main (String args[]) {
    // runs sampleText() and printMenu() methods
    String text = sampleText();
    System.out.println("You entered: " + text);
    printMenu(text);
}
}