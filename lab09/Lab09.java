/*
 * Brian Snyder
 * CSE 02
 * bts222
 * Lab09
 * 
 * Creates various methods to test understanding of arrays */

class Lab09 {
  
  public static void main(String args[]) {
    // initializes arrays
    int[] array0 = {0, 1, 2, 3, 4, 5, 6, 7};
    int[] array1 = copy(array0);
    int[] array2 = copy(array0);
    // runs print, and inverter methods for arrays
    inverter(array0);
    System.out.println("Array0:");
    print(array0);
    System.out.println("Array1:");
    inverter2(array1);
    print(array1);
    int[] array3 = inverter2(array2);
    System.out.println("Array2:");
    print(array3);
  }
  
  // method that takes an array and copies it to create a new array
  public static int[] copy(int[] list) {
    int[] newArray = new int[list.length];
    for (int i = 0; i < list.length; i++) {
      newArray[i] = list[i];
    }
    return newArray;
  }
  
  // method that takes an array and inverts it
  public static void inverter(int[] list) {
    for (int i = 0; i < list.length /2; i++) {
      int temp = list[i];
      list[i] = list[(list.length-i-1)];
      list[(list.length-i-1)] = temp;
    }
  }
  
  // method that creates a new array from a copy, inverts it, and return the array
  public static int[] inverter2(int[] list) {
    int[] copiedArray = copy(list);
    inverter(copiedArray);
    return copiedArray;
  }
  
  // method that prints the elements of an array
  public static void print(int[] list) {
    for (int i = 0; i < list.length; i++) {
      System.out.print(list[i] + " ");
    }
    System.out.println("");
  }
  
}  