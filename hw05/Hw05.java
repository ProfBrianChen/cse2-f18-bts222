/*
CSE 002 Hw05 Class
Brian Snyder
10/5/18
LIN: 856918280

This class generates a certain number of poker hands based off a user enter value.
The hands are generated randomly and then the probability of getting specific hands
is calculated, and the outputted */

import java.util.Scanner;

public class Hw05 {
  // Every Java program requires a main method
  public static void main(String[] args) {
    
    // Asks user for int, if the input is not valid, the code enters a loop until an int is eneterd
    Scanner myScanner = new Scanner(System.in);
    System.out.print("How many hands do you want generated: ");  
    while(!myScanner.hasNextInt()) {
      System.out.print("Invalid input, try again: ");
      myScanner.next();
    }
    int numOfHands = myScanner.nextInt();
    
    // Counters used to keep track of the different types of hands
    int fourOfAKind = 0;
    int threeOfAKind = 0;
    int twoPair = 0;
    int onePair = 0;
    int tempCount = 0;     // Temporary counter which is compared to the variable count while in the loop and then resets
    int count = 0;         // Counter that determines what type of pair a hand is
    int testCounter = 0;   // Counter used to diferntiate between when there's a 2 Pair and two 1 pairs
    
    // The following loops the # of times that the user wanted
    for (int i = 0; i < numOfHands; i++) {
      
      // A hand (represented by 5 #s) is initialized by setting them to random values 0-52 inclusively
      int num1 = (int)(Math.random() * 52) + 1;
      int num2 = (int)(Math.random() * 52) + 1;
      int num3 = (int)(Math.random() * 52) + 1;
      int num4 = (int)(Math.random() * 52) + 1;
      int num5 = (int)(Math.random() * 52) + 1;
      
      // If any of the numbers are exactly the same, all of the numbers are reset again, and this continues
      // until all of the 5 numbers are differnt
      while (num1 == num2 || num2 == num3 || num3 == num4 || num4 == num5 || num1 == num3 || num1 == num4 ||
             num1 == num5 || num2 == num4 || num2 == num5 || num3 == num5) {
        num1 = (int)(Math.random() * 52) + 1;
        num2 = (int)(Math.random() * 52) + 1;
        num3 = (int)(Math.random() * 52) + 1;
        num4 = (int)(Math.random() * 52) + 1;
        num5 = (int)(Math.random() * 52) + 1;
      }
      
      /*
      System.out.println(num1 % 13);  // Test code used to print the hands and see if the counters and everything
      System.out.println(num2 % 13);  // were working
      System.out.println(num3 % 13);
      System.out.println(num4 % 13);
      System.out.println(num5 % 13);
      System.out.println("------");
      */
      
      // Resets counters before cards are checked
      tempCount = 0;
      count = 0;
      testCounter = 0;
      
      // If card 1 is equal to any other card in the hand, card 1 is compared to all of the cards and the tempCount increases
      // each time a match is found
      if ((num1 % 13) == (num2 % 13) || (num1 % 13) == (num3 % 13) || (num1 % 13) == (num4 % 13) || (num1 % 13) == (num5 % 13)) {
        if ((num1 % 13) == (num2 % 13)) {
          tempCount++;
        }
        if ((num1 % 13) == (num3 % 13)) {
          tempCount++;
        }
        if ((num1 % 13) == (num4 % 13)) {
          tempCount++;
        }
        if ((num2 % 13) == num3) {
          tempCount++;
        }
      }
      
      // After card 1 is compared to everything, the count is compared to tempCount, and if 
      // count is smaller, count is est equal to tempCount and use later on while tempCount is reset for the next
      // card it will check
      if (tempCount > count) {
        count = tempCount;
      }
      tempCount = 0;
      
      // Same code as above except comparing card 2 to all other cards that have not already been compared
      if ((num2 % 13) == (num3 % 13) || (num2 % 13) == (num4 % 13) || (num2 % 13) == (num5 % 13)) {
        if (num2 % 13 == num3 % 13) {
          tempCount++;
        }
        if (num2 % 13 == num4 % 13) {
          tempCount++;
        }
        if (num2 % 13 == num5 % 13) {
          tempCount++;
        }
      }
      
      // Same idea as above where the code keeps count or sets it equal to tempCount, and then resets tempCount
      if (tempCount > count) {
        count = tempCount;
      
      // At this point, it's possible that card 1 and 2 can only have 1 pair, meaning a 2 Pair exists, so this if 
      // statement checks for that. If this if statement is entered, the counter corresponding to the # of twoPairs
      // is incremented while the testCounter is incremneted as well, which is used later on to keep 2 Pairs from being
      // counted as two 1 Pairs
      } else if (tempCount == 1 && count == 1) { 
        twoPair++;
        testCounter++;
      }
      tempCount = 0;
      
      // Same code as above except comparing card 3
      if (num3 % 13 == num4 % 13 || num3 % 13 == num5 % 13) {
        if (num3 % 13 == num4 % 13) {
          tempCount++;
        }
        if (num3 % 13 == num5 % 13) {
          tempCount++;
        }
      }
      
      // Same code as a above
      if (tempCount > count) {
        count = tempCount;
      } else if (tempCount == 1 && count == 1) {
        twoPair++;
        testCounter++;
      }
      tempCount = 0;
      
      // Same code as above except for comparing card 4, at this point card 5 does not need to be compared because
      // all of its potential matches were already compared
      if (num4 % 13 == num5 % 13) {
        tempCount++;
      }
      
      // Same concept as above
      if (tempCount > count) {
        count = tempCount;
      }
      tempCount = 0;
      
      // Code checks value of count, and based off its value, increments the corresponding counter
      if (count == 4) {
        fourOfAKind++;
      } else if (count == 3) {
        threeOfAKind++;
      } else if (count == 1 && testCounter < 1) { // if there is a match and there wasn't a 2 Pair in the hand
        onePair++;
      }
    }
      
      // Prints the # of each type of hand, done for testing
      System.out.println("4 of a Kind: " + fourOfAKind);
      System.out.println("3 of a Kind: " + threeOfAKind);
      System.out.println("2 Pair " + twoPair);
      System.out.println("1 Pair " + onePair);
      
      // Caclulates probability of each hand by taking the # of each hand and dividing it by total # of hands generated
      double numOfTimes = numOfHands;
      double probOf4OfAKind = fourOfAKind / numOfTimes;
      double probOf3OfAKind = threeOfAKind / numOfTimes;
      double probOfA2Pair = twoPair / numOfTimes;
      double probOfA1Pair = onePair / numOfTimes;
      
      // prints results
      System.out.println("The number of loops " + numOfHands);
      System.out.printf("The probability of a 4-of-a-kind: %5.3f%n", probOf4OfAKind);
      System.out.printf("The probability of a 3-of-a-kind: %5.3f%n", probOf3OfAKind);
      System.out.printf("The probability of a Two-Pair: %5.3f%n", probOfA2Pair);
      System.out.printf("The probability of a One-Pair: %5.3f%n", probOfA1Pair);
  }
}