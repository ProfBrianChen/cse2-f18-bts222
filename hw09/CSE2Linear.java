/* Brian Snyder
 * bts222
 * Hw09
 * CSE 02
 * 
 * This program has the user enter in grades for a class, and then
 * utilizes binary and linear search methods to find a specific grade
 * (when the arrays are scrambled and ordered) */

import java.util.Scanner;

public class CSE2Linear {
  
  // method that sorts through a list and checks if a specific value is present
  public static int[] linearSearch(int grade, int[] list) {
    int[] values = new int[2]; // array with two values, first represents if it was found, the second the # of iterations
    int found = 0; // 0 = found, -1 = not found
    int counter = 0; // # of iterations
    for (int i = 0; i < list.length; i++) {
      if (list[i] == grade) {
        break;
      } else {
        counter++;
      }
    }
    // if it went through every single time, then found = -1, which represents that the grade wasn't found
    if (counter == list.length) {
      found = -1;
    }
    values[0] = found;
    values[1] = counter;
    return values;
  }
  
  // method that randomly shuffles items in a list
  public static int[] scramble(int[] list) {
    int iterations = 100; // # of swithces
    // goes through loop and switches first value with random index
    for (int i = 0; i < iterations; i++) {
      int random = (int)(Math.random()*list.length);
      int temp = list[0];
      list[0] = list[random];
      list[random] = temp;
    }
    return list;
  }
  
  // method that uses binary search to find an element
  public static int[] binarySearch(int grade, int[] list) {
    int[] values = new int[2]; // same idea as above where first value in array represents if the value was found
                               // the second represents how many iterations was used
    int found = 0;
    int low = 0;
    int high = list.length-1;
    int mid = (low + high) / 2;
    int counter = 0;
    // while all of the elements haven't been found
    while (low <= high) {
      counter++;
      mid = (low + high) / 2; // middle is set
      if (grade < list[mid]) { // if mid is less than, change high
        high = mid-1;
      } else if (grade > list[mid]) { // if mid is greater change low
        low = mid+1;
      } else if (grade == list[mid]) { // else if mid is the grade, return
        values[0] = found;
        values[1] = counter;
        return values;
      }
    }
    // if everything else fails, element is not found
    found = -1;
    values[0] = found;
    values[1] = counter;
    return values;
  }
  
  // method that prints all the values of an array
  public static void print(int[] list) {
    for (int i = 0; i < list.length; i++) {
      System.out.print(list[i] + " ");
    }
    System.out.println("");
  }
  
  // main method 
  public static void main (String args[]) {
    Scanner scrn = new Scanner(System.in);
    boolean error = false; // boolean used so that if an error occurs, the program stops
    int[] grades = new int[15];
    System.out.println("Enter in 15 ascending ints for final grades in CSE2: ");
    // creates an arrya and then uses a loop to ask user for values
    // if values are not within range or not ascending, the program quits
    for (int i = 0; i < 15; i++) {
      if (scrn.hasNextInt()) {
        int num = scrn.nextInt();
        if (num < 0 || num > 100) {
          System.out.println("Error: Invalid grade entered");
          error = true;
          break;
          // for the first value, there is no previous value for it to be higher of 
        } else if (i == 0) {
            grades[i] = num;
          } else {
            if (grades[i-1] <= num) {
              grades[i] = num;
            } else {
              System.out.println("Error: Grades not entered in ascending order");
              error = true;
              break;
            }
          }
          // by default error pops up (if a string or other value was entered)
        } else {
          System.out.println("Error: Int was not enterd");
          error = true;
          break;
        }
      }
    // if there was an error, the program quits
    if (error == true) {
      return;
    }
    // prints grades, asks user for a value, and then uses binarysearch and linear search methods
    print(grades);
    System.out.print("Enter a grade to search for: ");
    int grade = 0;
    grade = scrn.nextInt();
    int[] iterations = new int[2];
    iterations = binarySearch(grade, grades);
    // if the value was not found, print the proper string
    if (iterations[0] == -1) {
      System.out.println(grade + " was not found in the list with " + iterations[1] + " iterations");
    } else {
      System.out.println(grade + " was found in the list with " + iterations[1] + " iterations");
    }
    grades = scramble(grades);
    System.out.println("Scrambled: ");
    print(grades);
    System.out.print("Enter a grade to search for: ");
    grade = scrn.nextInt();
    iterations = linearSearch(grade, grades);
    if (iterations[0] == -1) {
      System.out.println(grade + " was not found in the list with " + iterations[1] + " iterations");
    } else {
      System.out.println(grade + " was found in the list with " + iterations[1] + " iterations");
    }
  }
}