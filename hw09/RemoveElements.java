/* 
 * Brian Snyder
 * bts222
 * Hw09
 * CSE 02
 * 
 * Completing methods for the RemoveElements */

import java.util.Scanner;

public class RemoveElements {
  
  // method that removes any instances of an element from an array
  public static int[] remove(int[] list, int target) {
    for (int i = 0; i < list.length; i++) {
      // if an element is equal to the target, call method that removes element from array
      // and degresses i to account for list being one size smaller
      if (list[i] == target) {
        list = delete(list, i);
        i--;
      }
    }
    return list;
  }
  
  // method that deletes an index from an array and return a new array without this element
  public static int[] delete(int[] list, int pos) {
    int[] nums = new int[list.length-1];
    for (int i = 0; i < list.length-1; i++) {
      // if i is at the index or after, set num equal to the value one after i
      // done since if i was used, it would have the deleted element
      if (i == pos || i > pos) {
        nums[i] = list[i+1];
      } else {
        nums[i] = list[i];
      }
    }
    return nums;
  }
  
  // method that creates an array of ten randomly generated ints
  public static int[] randomInput() {
    int num = 0;
    int[] nums = new int[10];
    for (int i = 0; i < nums.length; i++) {
      num = (int)(Math.random() * 10);
      nums[i] = num;
    }
    return nums;
  }
  
  // main method that was provided
  public static void main(String [] arg)  {
    Scanner scan=new Scanner(System.in);
    int num[]=new int[10];
    int newArray1[];
    int newArray2[];
    int index,target;
    String answer="";
    do {
      System.out.print("Random input 10 ints [0-9]");
      num = randomInput();
      String out = "The original array is:";
      out += listArray(num);
      System.out.println(out);
 
      System.out.print("Enter the index ");
      index = scan.nextInt();
      newArray1 = delete(num,index);
      String out1="The output array is ";
      out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
      System.out.println(out1);
 
      System.out.print("Enter the target value ");
      target = scan.nextInt();
      newArray2 = remove(num,target);
      String out2="The output array is ";
      out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
      System.out.println(out2);
    
      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
      answer=scan.next();
    } while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
    String out="{";
    for(int j=0;j<num.length;j++){
      if(j>0){
        out+=", ";
      }
      out+=num[j];
    }
    out+="} ";
    return out;
  }
}
