/*
CSE 02 Pyramid Class
Brian Snyder
9/15/18
LIN: 856918280
Hw03 Program #2

This program prompts the user for the dimesions of a
pyramid, and returns the volume. */

// imports scanner
import java.util.Scanner;

public class Pyramid {
  // main method required for every Java program
  public static void main (String[] args) {
    
    // initializes scanner and asks user for dimensions of pyramid
    Scanner myScanner = new Scanner(System.in);
    System.out.print("The square side of the pyramid is (input length): ");
    double length = myScanner.nextDouble();
    System.out.print("The height of the pyramid is (input height): ");
    double height = myScanner.nextDouble();
    
    // converts volume with the formula: l*w*h /3  - length and width are the same since the base is a square
    double volume = ((Math.pow(length,2) * height) / 3);
    
    // prints results
    System.out.println("The Volume inside the pyramid is: " + volume);
  } // end of main method 
} // end of class