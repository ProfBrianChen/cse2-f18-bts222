/*
CSE 02 Convert Class
Brian Snyder
9/14/18
LIN: 856918280
Hw03 Program #1

This program asks the user for input of the number of acres of land
affected by a hurricane and how many inches of rain were dropped on
average. This program then converts the quantity of rain into cubic miles */

// imports Scanner
import java.util.Scanner;

public class Convert {
  // main method required for every Java program
  public static void main (String[] args) {
    
    // initializes scanner and asks user for inputs
    Scanner myScanner = new Scanner(System.in);
    System.out.print("Enter the affected area in acres: ");
    double acres = myScanner.nextDouble();
    System.out.print("Enter the rainfall in the affected area: ");
    double averageRainDropped = myScanner.nextDouble();
    
    // Converts data by converting inputs:
    // Acres -> Square miles
    // Finds cubic inches by # of inches in a square mile * amount of rain dropped
    // Cubic inches -> Gallons
    // Gallons -> Cubic Miles
    double squareMiles = (acres / 640); // converts acres to square miles
    double inchesPerSquareMile = Math.pow((5280*12), 2); // # of inches in a square mile
    double cubicInchesToGallons = 231; // 231 cubic inches = 1 gallon
    double gallonsToCubicMilesConversionFactor = 9.0817 * Math.pow(10,-13); // conversion rate of gallons to cubic miles
    double averageRainInCubicMiles = (((inchesPerSquareMile * averageRainDropped) / cubicInchesToGallons) * squareMiles 
                                      * gallonsToCubicMilesConversionFactor);
 
    // prints results
    System.out.println(averageRainInCubicMiles + " cubic miles");
    
    
    
  } // end of main method
} // end of class