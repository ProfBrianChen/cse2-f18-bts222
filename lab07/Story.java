/*
 * CSE 002
 * Brian Snyder
 * 10/26/18
 * LIN: 856918280
 * Lab07
 * 
 * This class utilizes multiple methods to generate random words to create a story */

import java.util.Random;

public class Story {
  
  // Class returns an adjective from a switch statement based off int num
  public static String generateAdj(int num) {
    String adj = " ";
    switch (num) {
      case 0:
        adj = "abrasive";
        break;
      case 1: 
        adj = "gamy";
        break;
      case 2: 
        adj = "handsome";
        break;
      case 3:
        adj = "brainy";
        break;
      case 4:
        adj = "embarrassed";
        break;
      case 5:
        adj = "tested";
        break;
      case 6:
        adj = "unruly";
        break;
      case 7: 
        adj = "smart";
        break;
      case 8:
        adj = "different";
        break;
      case 9:
        adj = "hurried";
        break;
      default:
        adj = " ";
        break;
    }
    return adj;
  }
  
  // this method returns a subject from a switch statement based off the value of num
  public static String generateSubject(int num) {
    String subject = " ";
    switch (num) {
      case 0:
        subject = "fox";
        break;
      case 1: 
        subject = "dog";
        break;
      case 2: 
        subject = "cat";
        break;
      case 3:
        subject = "moose";
        break;
      case 4:
        subject = "elephant";
        break;
      case 5:
        subject = "pelican";
        break;
      case 6:
        subject = "mouse";
        break;
      case 7:
        subject = "bird";
        break;
      case 8:
        subject = "flying squirrel";
        break;
      case 9:
        subject = "panda";
        break;
      default:
        subject = " ";
        break;
    }
    return subject;
  }
  
  // this method returns a verb from a switch statement based off the value of num
  public static String generateVerb(int num) {
    String verb = " ";
    switch (num) {
      case 0:
        verb = "celebrated";
        break;
      case 1: 
        verb = "dried";
        break;
      case 2: 
        verb = "announced";
        break;
      case 3:
        verb = "brushed";
        break;
      case 4:
        verb = "tipped";
        break;
      case 5:
        verb = "timed";
        break;
      case 6:
        verb = "stayed";
        break;
      case 7:
        verb = "ran";
        break;
      case 8:
        verb = "apologized";
        break;
      case 9:
        verb = "regreted";
        break;
      default:
        verb = " ";
        break;
    }
    return verb;
  }

  // this method returns a noun object from a switch statement based off the value of num
  public static String generateObject(int num) {
    String object = " ";
    switch (num) {
      case 0:
        object = "ball";
        break;
      case 1: 
        object = "toy";
        break;
      case 2: 
        object = "computer";
        break;
      case 3:
        object = "iPhone";
        break;
      case 4:
        object = "desk";
        break;
      case 5:
        object = "backpack";
        break;
      case 6:
        object = "chair";
        break;
      case 7:
        object = "tree";
        break;
      case 8:
        object = "cake";
        break;
      case 9:
        object = "coffee";
        break;
      default:
        object = " ";
        break;
    }
    return object;
  }
  
  // method that calls all of the generator methods and combines the output to make a string
  public static String sentenceGenerator() {
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    // System.out.println(randomInt); // used to test code and makes sure it works
    String adj = generateAdj(randomInt);
    String subject = generateSubject(randomInt);
    String object = generateObject(randomInt);
    String verb = generateVerb(randomInt);
    String sentence = "The " + adj + " " + subject + " " + verb + " " + object;
    return sentence;
  }
  
  // method that runs the sentence generator method in a loop, that outputs multiple sentences to make a paragragh
  public static String paragraphGenerator() {
    String paragraph = "";
    int numOfSentences = 4; // # of times loops iterates
    for (int i = 0; i < numOfSentences; i++) {
      String sentence = sentenceGenerator();
      paragraph += sentence + ". "; // adds sentences together to output one string
    }
    return paragraph;
  }
  
  // main method, which calls sentence and paragraph generator methods
  public static void main (String args[]) {
    System.out.println(sentenceGenerator());
    System.out.println(" ");
    System.out.println(paragraphGenerator());
    
  }
}