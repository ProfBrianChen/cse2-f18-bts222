/*
CSE 002 inputTest Class
Brian Snyder
10/5/18
LIN: 856918280
Lab 04

This class asks the user for info regarding a class, and if an
invalid input is enterd, the program uses loops to ask the user until
a valid input is enterd */

// imports Scanner
import java.util.Scanner;

public class inputTest {
  // main method required for every Java program
  public static void main (String[] args) {
    
    
    Scanner myScanner = new Scanner(System.in);
    String invalidInput = " ";  // string used to remove unwanted inputs
    String errorMessage = "Invalid input, please enter a valid input"; // default error message
    
    // Asks user for course number, if the value enterd is not an int, the error message is displayed
    // and the user is asked again, otherwise program breaks out of the loop and saves the int
    System.out.print("Enter in the course number: ");
    while (!myScanner.hasNextInt()) {
      System.out.println(errorMessage);;
      invalidInput = myScanner.next();
    }
    int classNum = myScanner.nextInt();
    
    // Same concept as above except that it asks for a string, so the while loop is entered if a 
    // int or double was enterd (aka anthing that's not a String)
    System.out.print("Enter in the department name: ");
    while(myScanner.hasNextInt() || myScanner.hasNextDouble()) {
      System.out.println(errorMessage);
      invalidInput = myScanner.next();
    }
    String classDepartment = myScanner.next();
    
    // Same code as above, just asking for # of times class meets
    System.out.print("Enter in the number of times the class meets per week: ");
    while (!myScanner.hasNextInt()) {
      System.out.println(errorMessage);
      invalidInput = myScanner.next();
    }
    int timesClassMeetsPerWeek = myScanner.nextInt();
    
    // Same code as above, just asking for time class starts
    System.out.print("Enter in the time the class starts ");
    while (!myScanner.hasNextInt()) {
      System.out.println(errorMessage);
      invalidInput = myScanner.next();
    }
    int timeClassStarts = myScanner.nextInt();
    
    // Same code as above, just asking for the instructor name
    System.out.print("Enter in the instructor name ");
    while (myScanner.hasNextInt() || myScanner.hasNextDouble()) {
      System.out.println(errorMessage);
      invalidInput = myScanner.next();
    }
    String instructorName = myScanner.next();
    
    // Same code as above, just asks for the number of students
    System.out.print("Enter in the number of students ");
    while (!myScanner.hasNextInt()) {
      System.out.println(errorMessage);
      invalidInput = myScanner.next();
    }
    int numStudents = myScanner.nextInt();
    
    // prints all of the stored values
    System.out.println("Course number: " + classNum);
    System.out.println("Department: " + classDepartment);
    System.out.println("Number of times the class meets per week: " + timesClassMeetsPerWeek);
    System.out.println("Time class starts " + timeClassStarts);
    System.out.println("Instructor: " + instructorName);
    System.out.println("Number of Students: " + numStudents);
    
    
  } // end of main method
} // end of class