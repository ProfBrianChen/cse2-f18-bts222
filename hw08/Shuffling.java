/*
 * Brian Snyder
 * bts222
 * CSE 02
 * HW 08
 * 
 * This program takes a deck of cards, and prints them, shuffles them,
 * and draws a hand from the deck and prints the hand */

import java.util.Scanner;

public class Shuffling { 
  
  public static void main(String[] args) { 
    Scanner scan = new Scanner(System.in); 
    //suits club, heart, spade or diamond 
    String[] suitNames={"C","H","S","D"};    
    String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
    String[] cards = new String[52]; 
    // Asks user for size of hand
    System.out.println("Enter in the size of hand");
    int handSize = scan.nextInt();
    // if user entered num < 0, this is not possible so hand size is set to 0 
    while (handSize < 0) {
      handSize = 0;
    }
    String[] hand = new String[handSize]; 
    // # of cards in a hand
    int numCards = handSize; 
    int again = 1; 
    int index = 51;
    // creates array of cards (by combining suit and rank strings)
    for (int i=0; i<52; i++){ 
      cards[i]=rankNames[i%13]+suitNames[i/13]; 
      //System.out.print(cards[i]+" "); // prints out card, does the same as printArray method
    }
    // prints array before and after its shuffled
    System.out.println();
    printArray(cards); 
    cards = shuffle(cards); 
    System.out.println("Shuffled");
    printArray(cards); 
    // while the user has entered '1', a hand is drawn
    while(again == 1){
      // if the hand is bigger than the deck, a hand is done for one deck, and another hand is 
      // done for however much above 52 the had is, and the two are combined
      if (numCards > 52) { 
        int numExtra = numCards - 52;
        hand = getHand(cards,index, 52); // hand made and printed for a whole deck
        System.out.println("Hand");
        printArray(hand);
        hand = getHand(cards,index,numExtra); // hand made and printed for extra amount
        printArray(hand);
        index = index - numCards;
        // if the index is going to go out of bounds, the index and cards are reset
        if (index < numCards) {
        index = 51;
        cards = shuffle(cards);
        }
        System.out.println("Enter a 1 if you want another hand drawn"); 
        again = scan.nextInt(); 
      } else {
        hand = getHand(cards,index,numCards);
        System.out.println("Hand: ");
        printArray(hand);
        // index decreases so that the cards drawn in the previous hands won't show up
        index = index - numCards;
        // if the index is going to go out of bounds, the index and cards are reset
        if (index < numCards) {
          index = 51;
          cards = shuffle(cards);
        }
        System.out.println("Enter a 1 if you want another hand drawn"); 
        again = scan.nextInt(); 
      }
    }  
  }
  
  // method that goes through a for loop to print the array
  public static void printArray(String[] cards) {
    for (int i = 0; i < cards.length; i++) {
      System.out.print(cards[i] + " ");
    }
    System.out.println();
  }
  
  // method that switches random indexes with the first index, essentially shuffling the cards
  public static String[] shuffle(String[] cards) {
    int index = 0;
    int numOfShuffles = 100;
    for (int i = 0; i < numOfShuffles; i++) {
      // random index chosen, value @ 0 saved using a temp variable, and values @ index 0 and random index are switched
      index = (int)(Math.random() * cards.length);
      String temp = cards[0];
      cards[0] = cards[index];
      cards[index] = temp;
    }
    return cards;
  }
  
  // method that takes the last five cards of the array and return them in their own array
  public static String[] getHand(String[] cards, int index, int numCards) {
    String[] hand = new String[numCards];
    for (int i = 0; i < numCards; i++) {
      hand[i] = cards[index];
      index--;
    }
    return hand;
  }
}