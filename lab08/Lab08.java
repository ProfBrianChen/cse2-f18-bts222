/*
 * Brian Snyder
 * CSE 02 Lab 08
 * bts222
 * 
 * Program that generates an array of random ints 0-99, and then
 * returns the occurances of each int in a second array */

public class Lab08 {
  // method that generates an array of random inputs the size of a given input
  public static int[] generateRandom(int size) {
    int[] nums = new int[size];
    for (int i = 0; i < size; i++) {
      nums[i] = (int)(Math.random() * 100); // random num 0-99 inclucively generated
    }
    return nums; // returns array
  }
  
  // method that counts the occurances of each number 0-99 within an inputed array
  public static int[] compareInts(int[] nums) {
    int[] numOfOccurances = new int[nums.length];
    int count = 0;
    // goes through inputed array and checks how many times the value of i appears
    // i will hold the value of 0-99, allowing it to check the occurances of all values
    for (int i = 0; i < numOfOccurances.length; i++) {
      for (int j = 0; j < nums.length; j++) {
        if (i == nums[j]) {
          count++;
        }
      }
      numOfOccurances[i] = count;
      count = 0; // resets counter for next value of i
    }
    return numOfOccurances; // return array
  }
  
  public static void main (String args[]) {
    // calls methods above and prints results
    int[] randomNums = generateRandom(100);
    int[] numOfOccurances = compareInts(randomNums);
    for (int i = 0; i < randomNums.length; i++) {
      System.out.print(i + " occurs " + numOfOccurances[i] + " times");
      System.out.println();
    }
  }
}