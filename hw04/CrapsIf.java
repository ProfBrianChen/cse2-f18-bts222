/*
CSE 02 CrapsIf Class
Brian Snyder
9/21/18
LIN: 856918280
HW 04

This program asks the user if they want a random dice roll
or if they want to choose the roll themselves. Then the program
will output the slang of that roll. This program will use if
statements to accomplish this task */

// imports Scanner
import java.util.Scanner;

public class CrapsIf {
  // main method required for every Java program
  public static void main (String[] args) {
    // initializes scanner
    Scanner myScanner = new Scanner(System.in);
    
    String slangTerm = ""; // string used to represent slang terms of dice rolls
    int dice1;             // integer to represent dice 1 roll
    int dice2;             // integer to represent dice 2 roll
    
    // Asks user for if they want random or to choose
    System.out.println("Would you like randomly cast dice or would you like to choose the two values");
    System.out.print("Enter '1 ' for random or '2' for choice: ");
    
    // saves input and uses a while loop to check if this input was valid
    // if the input was not valid, the computer asks the user to enter in
    // another value and this continues until the user enters in a valid input
    int userChoice = myScanner.nextInt();
    while (userChoice > 2 || userChoice < 1) {
      System.out.println("Invalid input, try again");
      System.out.print("Enter '1 ' for random or '2' for choicee: ");
      userChoice = myScanner.nextInt();
    }
    
    // if else that checks what input the user put
    // 1 = the program generates random integers (1,6) inclusively to represent a roll
    // 2 = the user inputs the values they want for their dice rolls
    if (userChoice == 1) {
      dice1 = (int)(Math.random() * 6 + 1);
      dice2 = (int)(Math.random() * 6 + 1);
    } else {
      System.out.print("Enter in the first die: ");
      dice1 = myScanner.nextInt();
      
      // same concept as above where a while loop only allows the
      // the user to enter a valid integer before continuing
      while (dice1 < 1 || dice1 > 6) {
        System.out.println("Invalid input, try again");
        System.out.print("Enter in the first die: ");
        dice1 = myScanner.nextInt();
      }
      
      // same as the while loop above, except for determing
      // the value of dice2
      System.out.print("Enter in the second die: ");
      dice2 = myScanner.nextInt();
      while (dice2 < 1 || dice2 > 6) {
        System.out.println("Invalid input, try again");
        System.out.print("Enter in the second die: ");
        dice2 = myScanner.nextInt();
      }
    }
      
    // Nested if else statement that checks what dice1 is, then what dice2 is
    // and then sets the string slangTerm to the appropriate slang term based off
    // the values of dice1 and dice2
      if (dice1 == 1) {
        if (dice2 == 1) {
          slangTerm = "Snake Eyes";
        } else if (dice2 == 2) {
          slangTerm = "Ace Deuce";
        } else if (dice2 == 3) {
          slangTerm = "Easy Four";
        } else if (dice2 == 4) {
          slangTerm = "Fever Five";
        } else if (dice2 == 5) {
          slangTerm = "Easy Six";
        } else {
          slangTerm = "Seven out";
        }
      } else if (dice1 == 2) {
        if (dice2 == 1) {
          slangTerm = "Ace Deuce";
        } else if (dice2 == 2) {
          slangTerm = "Hard Four";
        } else if (dice2 == 3) {
          slangTerm = "Fever Five";
        } else if (dice2 == 4) {
          slangTerm = "Easy Six";
        } else if (dice2 == 5) {
          slangTerm = "Seven out";
        } else if (dice2 == 6) {
          slangTerm = "Easy Eight";
        }
      } else if (dice1 == 3) {
        if (dice2 == 1) {
          slangTerm = "Easy Four";
        } else if (dice2 == 2) {
          slangTerm = "Fever Five";
        } else if (dice2 == 3) {
          slangTerm = "Hard Six";
        } else if (dice2 == 4) {
          slangTerm = "Seven out";
        } else if (dice2 == 5) {
          slangTerm = "Easy Eight";
        } else if (dice2 == 6) {
          slangTerm = "Nine";
        }
      } else if (dice1 == 4) {
        if (dice2 == 1) {
          slangTerm = "Fever Five";
        } else if (dice2 == 2) {
          slangTerm = "Easy Six";
        } else if (dice2 == 3) {
          slangTerm = "Seven out";
        } else if (dice2 == 4) {
          slangTerm = "Hard Eight";
        } else if (dice2 == 5) {
          slangTerm = "Nine";
        } else if (dice2 == 6) {
          slangTerm = "Easy Ten";
        }
      } else if (dice1 == 5) {
        if (dice2 == 1) {
          slangTerm = "Easy Six";
        } else if (dice2 == 2) {
          slangTerm = "Seven out";
        } else if (dice2 == 3) {
          slangTerm = "Easy Eight";
        } else if (dice2 == 4) {
          slangTerm = "Nine";
        } else if (dice2 == 5) {
          slangTerm = "Hard Ten";
        } else if (dice2 == 6) {
          slangTerm = "Yo-leven";
        }
      } else if (dice1 == 6) {
        if (dice2 == 1) {
          slangTerm = "Seven out";
        } else if (dice2 == 2) {
          slangTerm = "Easy Eight";
        } else if (dice2 == 3) {
          slangTerm = "Nine";
        } else if (dice2 == 4) {
          slangTerm = "Easy Ten";
        } else if (dice2 == 5) {
          slangTerm = "Yo-leven";
        } else if (dice2 == 6) {
          slangTerm = "Boxcars";
        }
      }
    
    // prints the value of both dice and their corresponding slang term
    System.out.println("Dice 1: " + dice1);
    System.out.println("Dice 2: " + dice2);
    System.out.println(slangTerm);
  } // end of main method 
} // end of class