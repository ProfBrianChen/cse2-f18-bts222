/*
CSE 02 CrapsSwitch Class
Brian Snyder
9/22/18
LIN: 856918280
HW 04

This program asks the user if they want a random dice roll
or if they want to choose the roll themselves. Then the program
will output the slang of that roll. This program will use switch statements 
to accomplish this task */

// imports Scanner
import java.util.Scanner;

public class CrapsSwitch {
  // main method required for every Java program
  public static void main (String[] args) {
    // initializes scanner
    Scanner myScanner = new Scanner(System.in);
    
    String slangTerm = "";  // string used to represent slang terms of dice rolls
    int dice1 = 0;          // integer to represent dice 1 roll
    int dice2 = 0;          // integer to represent dice 2 roll
    
    // Asks user for if they want random or to choose
    System.out.println("Would you like randomly cast dice or would you like to choose the two values");
    System.out.print("Enter '1 ' for random or '2' for choice: ");
    
    // saves input and uses a while loop to check if this input was valid
    // if the input was not valid, the computer asks the user to enter in
    // another value and this continues until the user enters in a valid input
    int userChoice = myScanner.nextInt();
    while (userChoice > 2 || userChoice < 1) {
      System.out.println("Invalid input, try again");
      System.out.print("Enter '1 ' for random or '2' for choice: ");
      userChoice = myScanner.nextInt();
    }
    
    // switch that checks what input the user put
    // 1 = the program generates random integers (1,6) inclusively to represent a roll
    // 2 = the user inputs the values they want for their dice rolls
    switch (userChoice) {
      case 1: 
        dice1 = (int)(Math.random() * 6 + 1);
        dice2 = (int)(Math.random() * 6 + 1);
        break;
      case 2:
        System.out.print("Enter in the first die: ");
        dice1 = myScanner.nextInt();
      
        // same concept as above where a while loop only allows the
        // the user to enter a valid integer before continuing
        while (dice1 < 1 || dice1 > 6) {
          System.out.println("Invalid input, try again");
          System.out.print("Enter in the first die: ");
          dice1 = myScanner.nextInt();
        }
      
        // same as the while loop above, except for determing
        // the value of dice2
        System.out.print("Enter in the second die: ");
        dice2 = myScanner.nextInt();
        while (dice2 < 1 || dice2 > 6) {
          System.out.println("Invalid input, try again");
          System.out.print("Enter in the second die: ");
          dice2 = myScanner.nextInt();
        }
        break;
      default:
        System.out.println("Error"); // default statement in case of error
        break;
    }
      
    // Nested switch statement that checks what dice1 is, then what dice2 is
    // and then sets the string slangTerm to the appropriate slang term based off
    // the values of dice1 and dice2  
    switch(dice1) {
      case 1:
        switch(dice2) {
          case 1: 
            slangTerm = "Snake Eyes";
            break;
          case 2:
            slangTerm = "Ace Deuce";
            break;
          case 3: 
            slangTerm = "Easy Four";
            break;
          case 4:
            slangTerm = "Fever Five";
            break;
          case 5: 
            slangTerm = "Easy Six";
            break;
          case 6:
            slangTerm = "Seven out";
            break;
          default:
            slangTerm = "Error";   // if there is an error, this default will run
            break;
        }
        break;
      case 2:
        switch (dice2) {
          case 1: 
            slangTerm = "Ace Deuce";
            break;
          case 2:
            slangTerm = "Hard Four";
            break;
          case 3:
            slangTerm = "Fever Five";
            break;
          case 4:
            slangTerm = "Easy Sizx";
            break;
          case 5: 
            slangTerm = "Seven out";
            break; 
          case 6: 
            slangTerm = "Easy Eight";
            break;
          default:
            slangTerm = "Error";    // if there is an error, this default will run
            break;
        }
        break;
      case 3:
        switch (dice2) {
          case 1:
            slangTerm = "Easy Four";
            break;
          case 2: 
            slangTerm = "Fever Five";
            break;
          case 3: 
            slangTerm = "Hard Six";
            break;
          case 4:
            slangTerm = "Seven out";
            break;
          case 5:
            slangTerm = "Easy Eight";
            break;
          case 6:
            slangTerm = "Nine";
            break;
          default:
            slangTerm = "Error";   // if there is an error, this default will run
            break;
        }
        break;
      case 4:
        switch (dice2) {
          case 1:
            slangTerm = "Fever Five";
            break;
          case 2: 
            slangTerm = "Easy Six";
            break;
          case 3:
            slangTerm = "Seven out";
            break;
          case 4:
            slangTerm = "Hard Eight";
            break;
          case 5: 
            slangTerm = "Nine";
            break;
          case 6:
            slangTerm = "Easy Ten";
            break;
          default:
            slangTerm = "Error";   // if there is an error, this default will run
            break;
        }
        break;
      case 5:
        switch(dice2) {
          case 1:
            slangTerm = "Easy Six";
            break;
          case 2: 
            slangTerm = "Seven out";
            break;
          case 3: 
            slangTerm = "Easy Eight";
            break;
          case 4:
            slangTerm = "Nine";
            break;
          case 5:
            slangTerm = "Hard Ten";
            break;
          case 6:
            slangTerm = "Yo-leven";
            break;
          default:
            slangTerm = "Error";   // if there is an error, this default will run
            break;
        }
        break;
      case 6:
        switch(dice2) {
          case 1:
            slangTerm = "Seven out";
            break;
          case 2: 
            slangTerm = "Easy Eight";
            break;
          case 3:
            slangTerm = "Nine";
            break;
          case 4:
            slangTerm = "Easy Ten";
            break;
          case 5:
            slangTerm = "Yo-leven";
            break;
          case 6:
            slangTerm = "Boxcars";
            break;
          default:
            slangTerm = "Error";  // if there is an error, this default will run
            break;
        }
        break;
      default:
        slangTerm = "Error"; // if there is an error, this default will run
        break;
    }  
    
    // prints the value of both dice and their corresponding slang term
    System.out.println("Dice 1: " + dice1);
    System.out.println("Dice 2: " + dice2);
    System.out.println(slangTerm);
    
  } // end of main method 
} // end of class