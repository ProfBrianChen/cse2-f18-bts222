/* 
CSE 02 Arithmetic Class
Brian Snyder
9/7/18
LIN: 856918280
Hw 02

Program allows you to compute the costs of items you buy,
including the PA sales tax */
public class Arithmetic {
// main method required for every Java Program
  public static void main(String[] args) {
    //intializes and declares variables
    int numPants = 3; // # of pairs of pants
    double pantsPrice = 34.98; // cost per pair of pants
   
    int numShirts = 2; // # of sweatshirts
    double shirtPrice = 24.99; // cost per shirt
    
    int numBelts = 1; // # of belts
    double beltCost = 33.99; // cost per belt
    
    double paSalesTax = 0.06; // the tax rate
    
    // calculates total cost of each item without tax
    double totalCostOfPants = numPants * pantsPrice;
    double totalCostsOfShirts = numShirts * shirtPrice;
    double totalCostsOfBelts = numBelts * beltCost;
    
    // calculates sales tax charged for each item (rounded to 2 decimal places)
    double pantsTax = ((int)(totalCostOfPants * paSalesTax *100)) / 100.0;
    double shirtsTax = ((int)(totalCostsOfShirts * paSalesTax*100)) / 100.0;
    double beltsTax = ((int)(totalCostsOfBelts * paSalesTax*100)) / 100.0;
    
    // calculates total cost of purchase without sales tax
    double total = totalCostsOfBelts + totalCostsOfShirts + totalCostOfPants;
    
    // calculates total amount of sales tax
    double totalTax = pantsTax + shirtsTax + beltsTax;
    totalTax = ((int)(totalTax * 100)) / 100.0;
    
    // calculates total amount for the transaction, including sales tax
    double transaction = total + totalTax;
    
    // prints results 
    System.out.println(numPants + " Pants for $" + pantsPrice + " =  " + totalCostOfPants);
    System.out.println("Sales Tax for Pants: $" + pantsTax);
    System.out.println(numShirts + " Shirts for $" + shirtPrice + " = " + totalCostsOfShirts);
    System.out.println("Sales Tax for Shits: $" + shirtsTax);
    System.out.println(numBelts + " Belts for $" + beltCost + " = " + totalCostsOfBelts);
    System.out.println("Sales Tax for Belts: $" + beltsTax);
    System.out.println("-----------------------------");
    System.out.println("Subtotal: $" + total);
    System.out.println("Tax: $" + totalTax);
    System.out.println("-----------------------------");
    System.out.println("Total: $" + transaction);
    
    
  }
}