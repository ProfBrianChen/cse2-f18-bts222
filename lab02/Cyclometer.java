/*
CSE 02 Cyclometer Class
Brian Snyder
9/7/18
LIN: 856918280
Lab 02     

Program imitates a cyclometer by recording the number of seconds passed and 
number of rotations for the front wheel, allowing distance and speed to be 
calculated    */

public class Cyclometer {
  // main method required for every Java program
  public static void main(String[] args) {
    // declares and initializes variables
    int secsTrip1 = 480; // time for first trip in seconds
    int secsTrip2 = 3220; // time for second trip in seconds
    int countsTrip1 = 1561; // # of rotations for first trip
    int countsTrip2 = 9037; // # of rotations for the secondtrip
    
    double wheelDiameter = 27.0; // diamter of the wheelDiameter
    double PI = 3.14159; // value of PI
    double feetPerMile = 5280; // # of feet in a mile
    double inchesPerFoot = 12; // # of inches in a foot
    double secondsPerMinute = 60; // # of seconds in a minute
    double distanceTrip1; // distance of Trip 1
    double distanceTrip2; // distance of Trip 2
    double totalDistance; // total distance of all trips
    
    // prints details of each trip
    System.out.println("Trip 1 took " + 
                      (secsTrip1/secondsPerMinute) + " minutes and had " +
                      countsTrip1 + " counts.");
    System.out.println("Trip 2 took " +
                      (secsTrip2/secondsPerMinute) + " minutes and had " +
                      countsTrip2 + " counts.");
    
    // calculates distances
    distanceTrip1 = countsTrip1*wheelDiameter*PI;
    // above gives distance in inches
    // (for each count, a rotation of the whel travels
    // the diamter in inches times PI)
    distanceTrip1 /= inchesPerFoot*feetPerMile; // Gives distance in miles
    distanceTrip2 = countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
    totalDistance = distanceTrip1 + distanceTrip2;
    
    // Prints out the output data
    System.out.println("Trip 1 was " + distanceTrip1 + " miles");
    System.out.println("Trip 2 was " + distanceTrip2 + " miles.");
    System.out.println("The total distance was " + totalDistance + " miles.");
    
  } // end of main method
} // end of class