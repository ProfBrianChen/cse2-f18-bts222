/*
CSE 02 Card Generator Class
Brian Snyder
9/21/18
LIN: 856918280
Lab 04

This program will generate a random number to represent a random selction
of a standard playing card */

public class CardGenerator {
  // main method required for every Java program
  public static void main (String[] args) {
    
    // randomly assings a number between 1 and 52 to integer
    int card = (int)(((Math.random() * 52) + 1));
    // intializes strings to be used to represent the name and suit of the card
    String cardName;
    String cardSuit;
    
    // nested if else statemnets that check which suit the card belongs in
    if (card <= 13) {
      cardSuit = "Diamonds";
    } else {
      if (card >= 14 && card <= 26) {
        cardSuit = "Clubs";
      } else {
        if (card >= 27 && card <= 39) {
          cardSuit = "Hearts";
        } else {
          cardSuit = "Spades";
        }
      } 
    }
    
    //System.out.println(card); // test statement for me to make sure code was working
    
    // mods integer by 13 so that only 13 cases are needed for the switch statement
    card = card % 13;
    // switch statement that returns the corresponding name to the card, based off it's original
    // number % 13 
    switch (card) {
      case 0: cardName = "King";
              break;
      case 1: cardName = "Ace";
              break;
      case 2: cardName = "Two";
              break;
      case 3: cardName = "Three";
              break;
      case 4: cardName = "Four";
              break;
      case 5: cardName = "Five";
              break;
      case 6: cardName = "Six";
              break;
      case 7: cardName = "Seven";
              break;
      case 8: cardName = "Eight";
              break;
      case 9: cardName = "Nine";
              break;
      case 10: cardName = "Ten";
              break;
      case 11: cardName = "Jack";
              break;
      case 12: cardName = "Queen";
              break;
      default: cardName = "The program failed"; // default in case there was an error in the program
              break;
    }
    // prints results
    System.out.println("You picked the " + cardName + " of " + cardSuit);
  } // end of main method
} // end of class