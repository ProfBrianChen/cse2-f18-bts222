/* CSE 02 Welcome Class
   Brian Snyder
   LIN: 856918280
   HW 01         Deadline: September 4th, 2018
   */

//creats class
public class WelcomeClass{
  // adds method
  public static void main(String[] args) {
    // prints required output and uses \\ because of escape characters
    System.out.println("  -----------");
    System.out.println("  | WELCOME |");
    System.out.println("  -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-b--t--s--2--2--2->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
    // prints short tweet-like bio
    System.out.println("Hi, my name is Brian Snyder. I am from Robbinsville, NJ");
    System.out.println("and my favorite sport is football.");
  }
}