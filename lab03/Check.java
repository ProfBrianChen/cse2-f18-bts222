/*
CSE 02 Check Class
Brian Snyder
9/14/18
LIN: 856918280
Lab 03

This program takes user input for the cost of the check, the tip, and the tip,
and then determines who much each person has to pay in order to pay the bill */

// imports Scanner
import java.util.Scanner;

public class Check {
  // main method required for every Java program
  public static void main (String[] args) {
    // declares scanner
    Scanner myScanner = new Scanner(System.in);
    // asks user for input and assigns inputs to variables
    System.out.print("Enter in the original cost of the check in the form of xx.xx: ");
    double checkCost = myScanner.nextDouble();
    System.out.print("Enter the percentage tip that you want to pay as a whole number (in the form xx): ");
    double tipPercent = myScanner.nextDouble();
    tipPercent /= 100; // convcerts percentage into a decimal value
    System.out.print("Enter the number of people who went out to dinner: ");
    int numPeople = myScanner.nextInt();
    
    // Calculates the amount each person pays
    double totalCost;
    double costPerPerson;
    int dollars, dimes, pennies; // whole dollar amount, dimes and pennies are for storing digits to the right of the decimal
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople; // gets whole dollar amount per person
    dollars = (int)costPerPerson;          // and drops decimals
    dimes = (int)(costPerPerson * 10) % 10;    // gets dime amount by using mod to get the remiander
    pennies = (int)(costPerPerson * 100) % 10; // gets penny amount by using mod to get the remainder
    
    // prints results
    System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);
    
  } // end of main method
} // end of class