/*
CSE 002 PatternB Class
Brian Snyder
10/12/18
LIN: 856918280
Lab 06

This class creates the specificed pattern B on the instructions
*/

// imports scanner
import java.util.Scanner;

public class PatternB {
  // every Java program requires a main method
  public static void main(String[] args) {
    
    // Asks user for integer
    Scanner scrn = new Scanner(System.in);
    System.out.print("Enter in an integer between 1-10: ");
    int num = 0;
    boolean invalidInput = true; // boolean used to determine if there is a valid input
    
    // while there is an invalid input, if there is not an int entered, program asks for an int and continues looping
    while (invalidInput) {
      if (!scrn.hasNextInt()) {
        System.out.print("Invalid input, try again: ");
        String clear = scrn.next();
      } else {
        // if an int is entered, but is not in the range, the loop continues, otherwise it breaks out of the loop
        num = scrn.nextInt();
          if (num < 1 || num > 10) {
            System.out.print("Invalid input, try again: ");
            continue;
          } else {
            invalidInput = false; // breaks out of loop
          }
      }
    }
    
    // Prints pattern B
    for (int i = 0; i < num+1; i++) {
      for (int j = 1; j <= num-i; j++) {
        System.out.print(j + " ");
      }
      System.out.println();
    }
  }
}