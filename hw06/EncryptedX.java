/* 
 * CSE 002 EncryptedX Class
 * Brian Snyder
 * 10/18/18
 * LIN: 856918280
 * Hw06
 * 
 * This class hides the message 'X' within a pattern */

import java.util.Scanner;

public class EncryptedX {
  // ever Java program requires a main method
  public static void main (String[] args) {
    
    // Asks user for valid integer
    Scanner scrn = new Scanner(System.in);
    System.out.print("Enter in an integer between 0-100: ");
    int size = 0;
    
    // Ff integer is not valid, while loop continues until valid number is entered
    boolean validInput = false;
    while (!validInput) {
      if (!scrn.hasNextInt()) {
        System.out.print("Enter in an integer between 0-100: ");
        String clear = scrn.next();
        continue;
      }
      size = scrn.nextInt();
      if (size > 100 || size < 0) {
        System.out.print("Enter in an integer between 0-100: ");
      } else {
        validInput = true; // Valid number being entered allows the code to exit while loop
      }
    }
    
    // Nester for loop that prints pattern
    for (int i = 0; i <= size; i++) {
      for (int j = 0; j <= size; j++) {
        // Prints '*' except on the diagonals
        if (i == j || size-i == j){
          System.out.print(" ");
        } else {
          System.out.print("*");
        }
      }
      // prints new line
      System.out.println();
    }
                                   
  } // end of main method
} // end of class